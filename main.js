 var app = angular.module('app', ['ui.router'])
app.config(function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/search');

    $stateProvider
        .state('home', {
            url: '/search',
            templateUrl: './components/home/home_comp.html',
            controller:'homeController'
        })
        .state('detail',{
            url:'/movie/:id',
            templateUrl:'./components/details/movie_detail.html',
            controller:'DetailController'
        })

      
});
