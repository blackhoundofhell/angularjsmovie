app.controller('homeController', function ($scope, homeSer) {
    $scope.movieName = "";
    $scope.movies = [];
    $scope.found=true;
    $scope.getMovies = function () {
        homeSer.getMovies($scope.movieName).then(
            r => {
                if(r.data.Response=='False'){
                    $scope.found=false;
                    $scope.movies=[];
                    return;

                }else{
                    $scope.found=true
                }
                $scope.movies = r.data.Search;
                console.log($scope.movies)
            }
        )
    }
})
.factory('homeSer', function ($http) {
        return {
            getMovies: function (name) {
                return $http.get('https://www.omdbapi.com/?s='+name+'&page=2&apikey=d17207ac')
            }
        }
    })

// http://www.omdbapi.com/?i=tt1416801&page=2&apikey=d17207ac